package io.scalac.chesschallenge

case class BoardDimensions(val width: Int, val height: Int) {
	assert(width >= 3, "Board has to be at least 3 squares width.")
	assert(height >= 3, "Board has to be at least 3 squares height.")
}