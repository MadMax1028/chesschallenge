package io.scalac.chesschallenge

object ChessPiece extends Enumeration {
	val KING, QUEEN, ROOK, KNIGHT, BISHOP, PAWN = Value
}