package io.scalac.chesschallenge

trait ChessPermutator {
	def allNonCollisionPermutations(dimensions: BoardDimensions, pieces: Seq[ChessPiece.Value]): Set[Board]
}